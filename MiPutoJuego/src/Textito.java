import java.io.Serializable;

public class Textito extends Sprite implements Serializable{
	
	public Textito(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.text=true;
		solid=false;
		unscrollable=true;
	}

}
