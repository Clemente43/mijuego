import java.io.Serializable;

/**
 * Clase Enemigos que estiende de Sprite que representa a los enemigos. Disparar, moverse, caer y golpear
 * @author David Clemente
 * @version 1.0
 *
 */
public class Enemigos extends Personas implements Serializable{
	

	public Enemigos(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		hp = 5;
		direccion = 'a';
		cooldown = 200;
		aceleracion=0;
		maxSalto =50;
		estat = 0;  //0 suelo, 1 subiendo  2 cayendo
	}
	/**
	  * El enemigo lanza un proyectil hacia la direccion en la que este mirando con un cooldown de 200 milisegundos
	  */
	public ProyectilE shoot2() {
		// TODO Auto-generated method stub
		if (cooldown == 200) {
			cooldown = 0;
			ProyectilE pE = new ProyectilE("ProyectilE", ((x1 + x2) / 2) - 50, ((y1 + y2) / 2)-50, ((x1 + x2) / 2), ((y1 + y2) / 2), "bala.png", direccion);
			return pE;
		} else {
			return null;
		}

	}
	/**
	 * Actualiza el cooldown
	  */
	public void update() {
		if (cooldown < 200)
			cooldown++;
	}
	@Override
	public void Danado() {
		hp--;
		if(direccion=='d') {
			x1-=5;x2-=5;
		}else {
			x1+=5;x2+=5;
		}
		if(hp==0) {
			Vikings.puntuacion+=100;
			delete();
		}
		
	}
	@Override
	public void movv(Field f) {
		if(estat==1) {
			if(maxSalto > 0) {
				y1-=aceleracion;y2-=aceleracion;
				if (aceleracion>0) {
					aceleracion--;
				}
			maxSalto--;
			}else {
				estat=2;
			}
		}
		else if(isGrounded(f)) {
			estat = 0;
			aceleracion = 15;
			maxSalto=50;
		}else {
			if (!isGrounded(f)) {
			estat = 2;
			y1+= aceleracion;y2+=aceleracion;
			if(aceleracion< 15) {
				aceleracion++;
			}
			if(y1>=800) {
				delete();
			}
		}
		}
	
}
	/**
	 * Movimiento aut�nomo de los enemigos
	 */
	public static void moveE() {
		for (Enemigos e : Vikings.m.enemigos) {
			if(e.name=="Enemigo"||e.name=="Pato") {
			if(e.x1-Ragnar.getRagnar().x2<400) {
			if(Ragnar.getRagnar().x1<e.x1&&Ragnar.getRagnar().x2<e.x2) {
				e.x1-=2;
				e.x2-=2;
			}else {
				e.x1+=2;
				e.x2+=2;
			}
				
			}
		}
		}
	}
	public static void saltoE() {
		for(Enemigos e : Vikings.m.enemigos) {
			if(e.estat==2) {
				e.jump(18, 50);
	}
		}
	}
	/**
	 * Los enemigos tienen gravedad
	 */
	public static void gravedadE() {
		for (Enemigos e : Vikings.m.enemigos) {
			e.movv(Vikings.f);
		}

	}
	/**
	 * disparos de los enemigos
	 */
	public static void disparoE() {
		for (Enemigos e : Vikings.m.enemigos) {
			e.update();
			ProyectilE pE = e.shoot2();
			if (pE != null) {
				Vikings.m.tirosE.add(pE);
			}
		}

	}
	}

