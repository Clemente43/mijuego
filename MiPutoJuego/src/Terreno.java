import java.io.IOException;
import java.io.Serializable;

public class Terreno extends Sprite implements Serializable{
	
	public Terreno(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
		terrain = true;		
	}
	public Terreno(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		terrain = true;		
	}
	static boolean caida=false;
	static boolean caida2=false;
	static boolean pinchito=false;
	public static void barranco() {
		
		for(Terreno t : Vikings.m.tierras) {
			if(t.name=="sueloabajo" && Ragnar.getRagnar().getCollisionType(t).contains("d")) {
				caida=true;
			}
			if(t.name=="sueloabajo2" && Ragnar.getRagnar().getCollisionType(t).contains("d")) {
				caida2=true;
			}
			if(caida==true&&t.name=="sueloabajo") {
			t.y1+=8;
			t.y2+=8;
			}else if (caida2==true&&t.name=="sueloabajo2") {
				t.y1+=8;
				t.y2+=8;
			}
			}

	}
	public static void chusa() throws InterruptedException, IOException {
		for(Terreno t : Vikings.m.tierras) {
			if(t.name=="suelopincho" && Ragnar.getRagnar().getCollisionType(t).contains("d")) {
				pinchito=true;
				t.currentImg=1;
				Ragnar.getRagnar().Danado();
				Ragnar.getRagnar().Danado();
				Ragnar.getRagnar().Danado();
			}
			
		}
	}


public static void nyamnyam() {
	for(Terreno t : Vikings.m.tierras) {
		if(t.name=="apple" && Ragnar.getRagnar().collidesWith(t)) {
			t.delete();
			Vikings.flag=true;
			Vikings.jugar=false;
			Vikings.jugar2=false;
			Vikings.jugar3=true;
		}
	}
}
}
//Vikings.r.getCollisionType(t).contains("d")