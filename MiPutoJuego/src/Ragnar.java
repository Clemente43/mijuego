import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Clase Ragnar que extiende de Sprite y representa al personaje principal del
 * juego. Disparar, atacar, agacharse, saltar, caer, golpear
 * 
 * @author David Clemente
 * @version 1.0
 *
 */
public class Ragnar extends Personas implements Serializable{

	static String[] cambioImagenProyectil = { "", "bala.png", "super.gif" };

	private Ragnar() {
		super("Ragnar", 175, 250, 450, 600, Vikings.cambioImagen);
		hp = 3;
		direccion = 'd';
		cooldown = 25;
		aceleracion = 0;
		agachado = false;
		agachamiento = 0;
		estat = 0; // 0 suelo, 1 subiendo 2 cayendo
		maxSalto = 50;
		collisionBox(75, 0, 0, 0);
	}
	private static Ragnar r = null;
	
	public static Ragnar getRagnar() {
		if(r==null) {
			r = new Ragnar();
			return r;
		}else {
			return r;
		}
		
	}
	/**
	 * El personaje lanza un proyectil hacia la direccion en la que este mirando con
	 * un cooldown de 25 milisegundos
	 */
	public Proyectil shoot() {
		// TODO Auto-generated method stub
		if (cooldown == 25) {
			cooldown = 0;
			Proyectil p = new Proyectil("Proyectil", (x1 + x2) / 2, (y1 + y2) / 2, ((x1 + x2) / 2) + 50,
					((y1 + y2) / 2) + 50, cambioImagenProyectil, direccion);
			return p;
		} else {
			return null;
		}

	}

	/**
	 * Actualiza el cooldown
	 */
	public void update() {
		if (cooldown < 25)
			cooldown++;
		if (direccion == 'd') {
			currentImg = 1;
		}
		if (direccion == 'a') {
			currentImg = 2;
		}
	}

	/**
	 * Nuestro personaje lanzar� un proyectil especial
	 */
	public Proyectil shootR() {
		// TODO Auto-generated method stub
		if (cooldown == 25) {
			cooldown = 0;
			Proyectil pR = new Proyectil("Proyectil", ((x1 + x2) / 2), ((y1 + y2) / 2), ((x1 + x2) / 2) + 300,
					((y1 + y2) / 2) + 1000, cambioImagenProyectil, direccion);
			return pR;
		} else {
			return null;
		}

	}

	public void marcadorV(HUD hud) {
		if (hp == 2) {
			hud.currentImg = 1;
		} else if (hp == 1) {
			hud.currentImg = 2;
		} else if (hp == 3) {
			hud.currentImg = 0;
		} else {
			hud.currentImg = 3;
		}
	}

	@Override
	public void Danado() throws InterruptedException, IOException {
		hp--;
		if (direccion == 'd') {
			x1 -= 5;
			x2 -= 5;
		} else {
			x1 += 5;
			x2 += 5;
		}
		if (hp == 0) {
//			delete();
			muerto();
			
		}
		}


	@Override
	public void movv(Field f) throws IOException {
		if (estat == 1) {
			if (maxSalto > 0) {
				y1 -= aceleracion;
				y2 -= aceleracion;
				if (aceleracion > 0) {
					aceleracion--;
				}
				maxSalto--;
			} else {
				estat = 2;
			}
		} else if (isGrounded(f) || collidesWith(Vikings.b)) {
			estat = 0;
			aceleracion = 15;
			maxSalto = 50;
		} else {
			if (!isGrounded(f) || !collidesWith(Vikings.b)) {
				estat = 2;
				y1 += aceleracion;
				y2 += aceleracion;
				if (aceleracion < 15) {
					aceleracion++;
				}
				if (y1 >= 800) {
//					delete();
					muerto();
				}
			}
		}
		
	}
	

	/**
	 * Detecta si Ragnar ha sufrido da�o al chocar con el enemigo
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public void RagnarTocao() throws InterruptedException, IOException {
		if (Vikings.w.getPressedKeys().contains('l')) {
			for (Enemigos enemigos : Vikings.m.enemigos) {
				if (collidesWith(enemigos)) {
					if (direccion == 'd') {
						x1 -= 5;
						x2 -= 5;
					} else {
						x1 += 5;
						x2 += 5;
					}
				}
			}
		} else {
			for (Enemigos enemigos : Vikings.m.enemigos) {
				if (collidesWith(enemigos) && !Vikings.w.getPressedKeys().contains('l')
						&& !getCollisionType(enemigos).contains("d")) {
					x1 = x1 - 50;
					x2 = x2 - 50;
					Danado();
				}
			}
		}

	}

	/**
	 * No hay gravedad y puedes ir hacia arriba con la 'w', ir hacia abajo con la
	 * 's', ir hacia la derecha con la 'd' e ir hacia la izquiera con la 'a',
	 * transformado en cuervo
	 */
		public void SinGravedad() {
			if(Vikings.jugar3==true) {
			currentImg = 10;
			}else if(Vikings.jugar3==false){
			currentImg=7;
			}
			if (Vikings.w.getPressedKeys().contains('w')) {
				moveArriba(Vikings.f);
			}
			if (Vikings.w.getPressedKeys().contains('s')) {
				moveAbajo(Vikings.f);

			}
		}

	/**
	 * NUestro personaje tiene gravedad
	 * @throws IOException 
	 */
	public void gravedad() throws IOException {
		movv(Vikings.f);
	}

	/**
	 * cambia el booleano trans
	 */
	public void tranformacion() {
		if (Vikings.trans == true) {
			Vikings.trans = false;
		} else {
			Vikings.trans = true;
		}

	}

	public void choque() {
		if (isOnColumn(Vikings.f)) {

			getSided(Vikings.f);

		}
//		for (Enemigos e : Vikings.m.enemigos)
//			if (getCollisionType(e).contains("d")) {
//				y1 -= 20;
//				y2 -= 20;
//			}
	}
	public void muerto() throws IOException {
		Date d = new Date();
		Vikings.flag = true;
		Vikings.jugar=true;
		Field f = new Field();
		Window w = new Window(f);
		String nom = w.showInputPopup("Nombre:");
		String s = d+"";
		Ranking p = new Ranking (nom, Vikings.puntuacion , s);
		Vikings.alr.add(p);
		Ranking.guardarScore("Ranking.csv");
		Vikings.puntuacion=0;

	}
}
