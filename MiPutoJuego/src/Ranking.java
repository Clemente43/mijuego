import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Ranking implements Comparable{
	String nombre;
	int puntuacion;
	String data;

	public Ranking(String nombre, int puntuacion, String data) {
		super();
		this.nombre = nombre;
		this.puntuacion = puntuacion;
		this.data = data;
	}

	public static void guardarScore(String path) throws IOException {
		File f = new File(path);
		FileWriter fw = new FileWriter(f, true);
		BufferedWriter bw = new BufferedWriter(fw);
		// escribir en buffer
		for (Ranking r : Vikings.alr) {
			String s = r.nombre + "," + r.puntuacion + "," + r.data;
			bw.write(s);
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}
	
	public static void leerScore(String path) throws IOException {
		File f = new File(path);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		Vikings.alr = new ArrayList<>();
		while(br.ready()) {
			String s = br.readLine();
			String[] spl = s.split(",");
			Ranking r = new Ranking(spl[0], Integer.parseInt(spl[1]), spl[2]);
			Vikings.alr.add(r);
		}
		Collections.sort(Vikings.alr); 
		Collections.reverse(Vikings.alr);
		br.close();
		for (Ranking r : Vikings.alr) {
			System.out.println(r);
		}
	}

	@Override
	public int compareTo(Object o) {
		Ranking otro = (Ranking) o;
		return this.puntuacion-otro.puntuacion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ranking [nombre=");
		builder.append(nombre);
		builder.append(", puntuacion=");
		builder.append(puntuacion);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}
	

}
