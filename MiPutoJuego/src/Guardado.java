import java.io.Serializable;
import java.util.ArrayList;

public class Guardado extends Vikings implements Serializable{

		int x1;
		int x2;
		int y1;
		int y2;
		int puntuacion;
		int hp;
		ArrayList<Enemigos> enemigos;
		ArrayList<Terreno> tierras;
		ArrayList<Proyectil> tiros;
		ArrayList<ProyectilE> tirosE;
		boolean jugar;
		boolean ins;
		boolean salir;
		boolean portal;
		boolean apple;
		boolean flag;
		boolean jugar2;
		boolean jugar3;
		boolean caida;
		boolean caida2;
		boolean trans;
		int bx1;
		int bx2;
		int by1;
		int by2;
		int scrollx;
		int scrolly;
		
		

		

		public Guardado(int x1, int x2, int y1, int y2, int puntuacion, int hp, ArrayList<Enemigos> enemigos,
				ArrayList<Terreno> tierras, ArrayList<Proyectil> tiros, ArrayList<ProyectilE> tirosE, boolean jugar,
				boolean portal, boolean flag, boolean jugar2, boolean jugar3, boolean trans, int scrollx, int scrolly) {
			super();
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
			this.puntuacion = puntuacion;
			this.hp = hp;
			this.enemigos = enemigos;
			this.tierras = tierras;
			this.tiros = tiros;
			this.tirosE = tirosE;
			this.jugar = jugar;
			this.portal = portal;
			this.flag = flag;
			this.jugar2 = jugar2;
			this.jugar3 = jugar3;
			this.trans = trans;
			this.scrollx = scrollx;
			this.scrolly = scrolly;
		}





		public Guardado(int x1, int x2, int y1, int y2, int puntuacion, int hp, ArrayList<Enemigos> enemigos,
				ArrayList<Terreno> tierras, boolean jugar, boolean ins, boolean salir, boolean apple, boolean flag,
				boolean jugar2, boolean jugar3, boolean caida, boolean caida2, int bx1, int bx2, int by1, int by2,
				int scrollx, int scrolly) {
			super();
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
			this.puntuacion = puntuacion;
			this.hp = hp;
			this.enemigos = enemigos;
			this.tierras = tierras;
			this.jugar = jugar;
			this.ins = ins;
			this.salir = salir;
			this.apple = apple;
			this.flag = flag;
			this.jugar2 = jugar2;
			this.jugar3 = jugar3;
			this.caida = caida;
			this.caida2 = caida2;
			this.bx1 = bx1;
			this.bx2 = bx2;
			this.by1 = by1;
			this.by2 = by2;
			this.scrollx = scrollx;
			this.scrolly = scrolly;
		}





		public Guardado(int x1, int x2, int y1, int y2, int puntuacion, int hp, ArrayList<Enemigos> enemigos,
				ArrayList<Terreno> tierras, ArrayList<ProyectilE> tirosE, boolean jugar3, int scrollx, int scrolly) {
			super();
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
			this.puntuacion = puntuacion;
			this.hp = hp;
			this.enemigos = enemigos;
			this.tierras = tierras;
			this.tirosE = tirosE;
			this.jugar3 = jugar3;
			this.scrollx = scrollx;
			this.scrolly = scrolly;
		}
		
}
