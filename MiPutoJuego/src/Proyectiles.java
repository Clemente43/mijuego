import java.io.Serializable;

public abstract class Proyectiles extends Sprite implements Serializable{
	
	
		char direccion;
	public Proyectiles(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		
	}
	public Proyectiles(String name, int x1, int y1, int x2, int y2, String[] cambioImagenProyectil, char dir) {
		super(name, x1, y1, x2, y2, cambioImagenProyectil);
	direccion=dir;
	}
	
	public abstract void colision (Field f);
	
	public abstract void move(Field f);
	
	
}
