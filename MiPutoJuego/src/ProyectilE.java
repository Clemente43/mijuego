import java.io.IOException;
import java.io.Serializable;

/**
 * Clase ProyectilE que extiende de Sprite y representa los proyectiles que lanzan los enemigos
 * @author David Clemente
 * @version 1.0
 *
 */
	public class ProyectilE extends Proyectiles implements Serializable{

		char direccion;
		
		public ProyectilE(String name, int x1, int y1, int x2, int y2, String path) {
			super(name, x1, y1, x2, y2, path);
			// TODO Auto-generated constructor stub
		}

		public ProyectilE(String name, int x1, int y1, int x2, int y2, String path, char dir) {
			super(name, x1, y1, x2, y2, path);
			direccion = dir;
			
		}
		/**
		  * Mira si el proyectil del enemigo ha colisionado con algo y si ha colisionado lo borra
		  */
		public void colision (Field f) {
			Sprite s = firstCollidesWithField(f);
					//si ha colisionado con algo
					if(s!=null) {
					//si s es de Clase Enemigo.
						if(s instanceof Ragnar) {
							//casteo de enemigo
							Ragnar r = (Ragnar) s;
						}
						if(!(s instanceof Enemigos)) {
							delete();
						}
					}
		}
		/**
		  * Movimiento del proyectil del enemigo
		  */
		public void move(Field f) {
			if(direccion=='a') {
				x1-=15;x2-=15;
			}else {
				x1+=15;x2+=15;
			}
			
		}
		public static void colisionP() throws InterruptedException, IOException {
			for (ProyectilE pE : Vikings.m.tirosE) {
				if (Vikings.w.getPressedKeys().contains('l')) {
					if (Ragnar.getRagnar().collidesWith(pE)) {
						pE.delete();
					}
				} else {
					if (Ragnar.getRagnar().collidesWith(pE)) {
						Ragnar.getRagnar().Danado();
						if (Ragnar.getRagnar().direccion == 'd') {
							Ragnar.getRagnar().x1 -= 5;
							Ragnar.getRagnar().x2 -= 5;
						} else {
							Ragnar.getRagnar().x1 += 5;
							Ragnar.getRagnar().x2 += 5;
						}
						pE.delete();
					}

				}

			}

		}

	}

