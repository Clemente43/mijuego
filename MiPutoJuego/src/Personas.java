import java.io.IOException;
import java.io.Serializable;

public abstract class Personas extends Sprite implements Serializable{
	public Personas(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		
	}
	public Personas(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
		
	}
	
	int hp;
	char direccion;
	int cooldown;
	String[] cambioImagenProyectil;
	int maxSalto;
	int aceleracion;
	int estat;
	boolean agachado=false;
	int agachamiento=0;	
	
	public abstract void Danado () throws InterruptedException, IOException;
	/**
	  * El personaje lanza un proyectil hacia la direccion en la que este mirando con un cooldown de 25 milisegundos
	  */
	public Proyectil Disparar() {
			if(cooldown==25) {
				cooldown=0;
				Proyectil p = new Proyectil("Proyectil", (x1+x2)/2, (y1+y2)/2, ((x1+x2)/2)+50, ((y1+y2)/2)+50, cambioImagenProyectil, direccion);
				return p;
			}else {
				return null;
			}
			
		}
	/**
	  * Nuestro personaje se mover� hacia la derecha si no hay una plataforma que se lo impida   
	  */
	public void moveDer(Field f) {
		if(isOnColumn(f)) {
			
			getSided(f);

		}else {
		x1+=5;x2+=5;
		direccion='d';
		}
	}
	/**
	  * Nuestro personaje se mover� hacia la izquierda si no hay una plataforma que se lo impida   
	  */
	public void moveIzq(Field f) {

		if(isOnColumn(f)) {
			
			getSided(f);
		}else {
		x1-=5; x2-=5;
		direccion='a';
	}
	}
	/**
	 * Nuestro personaje har� un ataque con la espada hacia la direccion que est� con un cooldown de 25 milisegundos
	 * @throws InterruptedException 
	  */
	public void espadazo() throws InterruptedException {
		if(cooldown==25) {
			cooldown=0;
		if(direccion=='d') {
			currentImg=3;
			x1=x1+100; x2=x2+100;
			for (Enemigos enemigos : Vikings.m.enemigos) {
				if (Ragnar.getRagnar().collidesWith(enemigos)) {
					enemigos.Danado();
				}
			}
			Thread.sleep(100);
			x1=x1-100; x2=x2-100;			
			
		}else if(direccion=='a') {
			currentImg=4;
			x1=x1-100; x2=x2-100;
			for (Enemigos enemigos : Vikings.m.enemigos) {
				if (Ragnar.getRagnar().collidesWith(enemigos)) {
					enemigos.Danado();
				}
			}
			Thread.sleep(100);
			x1=x1+100; x2=x2+100;
			
		}
	}
	}
	/**
	 * Nuestro personaje se mover� hacia arriba
	  */
	public void moveArriba(Field f) {
		if(isOnCeiling(f)) {
			
		}else {
		y1-=10;y2-=10;
		}
	}
	/**
	 * Nuestro personaje se mover� hacia abajo
	 */
	public void moveAbajo(Field f) {
		if(isGrounded(f)) {
			
		}else {
		y1+=10;y2+=10;
		}
	}
	/**
	 * El personaje saltara
	 * 
	 * @param acc aceleracion con la que baja nuestro personaje 
	 * @param salto la distancia maxima que subira nuestro personaje cuando salte y cuando empezara a bajar
	 */
	public void jump(int acc, int salto) {
		if(estat == 0) {
			estat = 1;
			aceleracion=acc;
			maxSalto=salto;
		}
	}
	/**
	  * El personaje caera con aceleracion si esta en el aire en el punto mas alto de su salto
	 * @throws IOException 
	  */
	public abstract void movv(Field f) throws IOException;
	
	public void pabajo() {
		if(cooldown==25 && agachamiento > -1) {
		cooldown=0;
		y1+=50;
		agachamiento--;
	}
	}

	public void parriba() {
		if(cooldown==25 && agachamiento < 0) {
			cooldown=0;
			y1-=50;
			agachamiento++;

		}
		}
	public abstract void update();
	
	}

