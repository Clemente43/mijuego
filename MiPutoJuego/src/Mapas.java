import java.io.Serializable;
import java.util.ArrayList;

public class Mapas implements Serializable{
	
	ArrayList<Enemigos> enemigos = new ArrayList<>();
	/**
	 * ArrayList de la clase Terreno donde se a�adiran todos las plataformas que
	 * a�adamos
	 */
	ArrayList<Terreno> tierras = new ArrayList<>();
	/**
	 * ArrayList de la clase Proyectil donde se a�adiran todos los proyectiles que
	 * lance nuestro personaje
	 */
	ArrayList<Proyectil> tiros = new ArrayList<>();
	/**
	 * ArrayList de la clase ProyectilE donde se a�adiran todos los proyectiles que
	 * lacen los enemigos
	 */
	ArrayList<ProyectilE> tirosE = new ArrayList<>();
	
}
