import java.io.Serializable;

public class Barquito extends Sprite implements Serializable{

	public Barquito(String name, int x1, int y1, int x2, int y2, String string) {
		super(name, x1, y1, x2, y2, string);
	}
	@Override
	public void collisionBox(int x1, int x2, int y1, int y2) {
		super.collisionBox(x1, x2, y1, y2);
	}
	
	public void move() {
		x1+=2; x2+=2;
		if(this.collidesWith(Ragnar.getRagnar())) {
			x1+=3; x2+=3;
			Ragnar.getRagnar().x1+=5; Ragnar.getRagnar().x2+=5;
			if (x1 > Vikings.maxscroll) {
				// scroll positiu: A l'esquerra
				Vikings.f.scroll(-5, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				Vikings.minscroll += 5;
				Vikings.maxscroll += 5;
			}
			
			
	}
		
	}
	
	
}
