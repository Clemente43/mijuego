
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

/**
 * Juego de plataforma con tematica de Vikingos donde puedes saltar, ir hacia la
 * izquierda, ir hacia la derecha, transformarte en cuervo y lanzar proyectiles.
 * 
 * @author David Clemente
 * @version 1.0
 *
 */
public class Vikings {
	/**
	 * booleano que determina si nuestro personaje se transforma en cuervo o no
	 */
	static boolean trans = false;
	static boolean cargar = false;
	static boolean pause = true;
	static boolean jugar = false;
	static boolean ins = false;
	static boolean salir = false;
	static boolean portal = false;
	static boolean apple = false;
	static boolean jugar2 = false;
	static boolean jugar3 = false;
	static boolean salirjugar = false;
	static int puntuacion = 0;
	/**
	 * booleano que determina si el juego sigue o se acaba
	 */
	static boolean flag = false;
	static Field f = new Field();
	static Window w = new Window(f);
	static Scanner sc = new Scanner(System.in);
	/**
	 * Array de Strings donde estan todos los Sprites de nuestro personaje
	 */
	static String[] cambioImagen = { "", "RagnarCaminaToBien2.gif", "RagnarIzquierda.png", "EspadazoDerecha.png",
			"EspadazoIzquierda.png", "Escudo.png", "super.gif", "cuervo.gif", "RagnarAgachado!.png",
			"RagnarAgachadoIzquierda.png", "RagnarSwim!.png" };
	static String[] vidas = { "Vidas3.png", "Vidas2.png", "Vidas1.png", "" };
	static String[] suelitos = { "SueloTierra.png", "soylabomba.png" };
	static Barquito b = new Barquito("Barco", 0, 500, 270, 700, "barco.png");

	static Textito t = new Textito("Score", 1250, 0, 1400, 50, "Puntos: " + puntuacion);

	/**
	 * Declaracion de nuestro personaje con el nombre, x1, y1, x2, y2, y el Array de
	 * Strings de sus imagenes
	 */
//	static Ragnar r = new Ragnar("Ragnar", 100, 250, 250, 400, cambioImagen);
	static Mapas m = new Mapas();
	static ArrayList<Botones> menu = new ArrayList<>();
	static HUD hud = new HUD("vidas", 25, 25, 175, 75, vidas);
	static Random random = new Random();
	static String[] musicas = { "", "" };
	static int cancionAct = 0;
	static int minscroll = 250;
	static int maxscroll = 300;
	static ArrayList<Ranking> alr = new ArrayList<>();

	static int x;
	static int y;

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		int contador = 0;

		while (!salir) {
			listasVacias();
			menu();
			while (!salirjugar) {
				if (jugar) {
					listasVacias();
					jugar();

				} else if (jugar2) {
					listasVacias();
					jugar2();

				} else if (jugar3) {
					listasVacias();
					jugar3();
				}
			}
		}
	}

	private static void jugar3() throws InterruptedException, IOException, ClassNotFoundException {
		Ragnar r = Ragnar.getRagnar();
		r.x1 = 175;
		r.x2 = 250;
		r.y1 = 450;
		r.y2 = 600;
		r.hp = 3;
		r.useImgArray = true;
		hud.useImgArray = true;
//		w.playMusic("");
		r.currentImg = 10;
		jugar = false;
		for (Terreno t : Vikings.m.tierras) {
			t.currentImg = 0;
		}
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		trans = false;
		jugar = false;
		jugar2 = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		
		m.enemigos.add((new Enemigos("Pulpo", 7800, 300, 8000, 650, "pulpo.png")));

		m.tierras.add((new Terreno("suelo", 550, 0, 600, 400, suelitos)));
		m.tierras.add((new Terreno("suelo", 950, 0, 1000, 400, suelitos)));
		m.tierras.add((new Terreno("suelo", 1650, 500, 1700, 1000, suelitos)));
		m.tierras.add((new Terreno("suelo", 2000, 0, 2050, 300, suelitos)));
		m.tierras.add((new Terreno("suelo", 2200, 500, 2500, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 2850, 0, 3000, 500, suelitos)));
		m.tierras.add((new Terreno("suelo", 3500, 0, 3600, 600, suelitos)));
		m.tierras.add((new Terreno("suelo", 3800, 500, 4000, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 4200, 400, 4600, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 5000, 0, 5100, 400, suelitos)));
		m.tierras.add((new Terreno("suelo", 5000, 600, 5100, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 5200, 0, 5500, 300, suelitos)));
		m.tierras.add((new Terreno("suelo", 5200, 500, 5500, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 5800, 0, 6000, 200, suelitos)));
		m.tierras.add((new Terreno("suelo", 5800, 400, 6000, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 6300, 0, 6400, 500, suelitos)));
		m.tierras.add((new Terreno("suelo", 6300, 700, 6400, 900, suelitos)));
		m.tierras.add((new Terreno("suelo", 6600, 0, 7000, 300, suelitos)));
		m.tierras.add((new Terreno("suelo", 6600, 500, 7000, 900, suelitos)));
		
		
		if (cargar) {
			cargar2();
		}
		while (!flag) {
			f.background = "fondobikini.jpg";
			t = new Textito("Score", 1250, 0, 1400, 50, "Puntos: " + puntuacion);
			r.choque();
			Enemigos.moveE();
			r.update();
			patitoMuerto();
			input();
			Enemigos.disparoE();
			for (ProyectilE pE : m.tirosE) {
				pE.move(f);
			}
			ProyectilE.colisionP();
			r.marcadorV(hud);
			Terreno.nyamnyam();
			r.SinGravedad();
			r.RagnarTocao();
			GoPortal();
			GoApple();
			fin();
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			sprites.addAll(m.tirosE);
			sprites.add(hud);
			sprites.add(t);
			for (Iterator<Sprite> iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
	}

	private static void fin() throws IOException {
		for (Enemigos e : Vikings.m.enemigos) {
			if (e.name=="Pulpo") {
				if(e.delete) {
					flag=true;
					f.background= "FIN.gif";
					Ragnar.getRagnar().muerto();
				
			}
			}
		}
	}

	private static void jugar2() throws InterruptedException, IOException, ClassNotFoundException {
		Ragnar r = Ragnar.getRagnar();
		r.x1 = 175;
		r.x2 = 250;
		r.y1 = 450;
		r.y2 = 600;
		r.hp = 3;
		r.useImgArray = true;
		hud.useImgArray = true;
//		w.playMusic("");
		r.currentImg = 1;
		jugar = false;
		for (Terreno t : Vikings.m.tierras) {
			t.currentImg = 0;
		}
		b = new Barquito("Barco", 0, 725, 300, 800, "barco.png");
		b.collisionBox(0, 50, 200, 0);
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		// m.tiros = new ArrayList<>();
		// m.tirosE = new ArrayList<>();
		trans = false;
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		m.enemigos.add((new Enemigos("Pato", 1850, 550, 1950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 4150, 550, 4250, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 5850, 550, 5950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("PatoF", 7800, 400, 7950, 620, "Ene.png")));
//		m.tierras.add((new Terreno("suelo", 100, 400, 600, 470, "SueloTierra.png")));
//		m.tierras.add((new Terreno("suelo", 400, 600, 1000, 650, "SueloTierra.png")));
		m.tierras.add((new Terreno("suelo", 1000, 650, 1170, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo", 1170, 650, 1400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 1400, 650, 2000, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo2", 4000, 650, 4300, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 3400, 650, 4000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5200, 650, 5390, 700, suelitos)));
		m.tierras.add((new Terreno("suelopincho", 5390, 650, 5400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5400, 650, 6000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 7400, 650, 8000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 8100, 650, 8300, 700, suelitos)));
		if (cargar) {
			cargar2();
		}
		while (!flag) {
			f.background = "mar.jpg";
			t = new Textito("Score", 1250, 0, 1400, 50, "Puntos: " + puntuacion);
			b.move();
			r.choque();
			Enemigos.moveE();
			Terreno.barranco();
			Terreno.chusa();
			r.update();
			// Enemigos.disparoE();
			patitoMuerto();
			input();
			Enemigos.gravedadE();
			// ProyectilE.colisionP();
			Enemigos.saltoE();
			r.marcadorV(hud);
			Terreno.nyamnyam();
			r.gravedad();
			r.RagnarTocao();
			GoPortal();
			GoApple();
//			for (Proyectil p : m.tiros) {
//				p.move(f);
//			}
//			for (ProyectilE pE : m.tirosE) {
//				pE.move(f);
//			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(b);
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			sprites.add(t);
			// sprites.addAll(m.tiros);
			// sprites.addAll(m.tirosE);
			sprites.add(hud);
			for (Iterator<Sprite> iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
	}

	private static void patitoMuerto() {
		for (Enemigos e : m.enemigos) {
			if (Ragnar.getRagnar().getCollisionType(e).contains("d")) {
				Ragnar.getRagnar().y1 -= 250;
				Ragnar.getRagnar().y2 -= 250;
				e.Danado();
				e.Danado();
				e.Danado();
			}
			
		}
	}

//	public static void RagnarMuerto() {
//		if (flag == true) {
//			menu.add((new Botones("Jugar", 0, 300, 800, 600, "JUGAR.png")));
//		}
//	}

	private static void listasVacias() {
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		m.tiros = new ArrayList<>();
		m.tirosE = new ArrayList<>();
		menu = new ArrayList<>();
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		jugar2 = false;
		Terreno.caida = false;
		Terreno.caida2 = false;
		f.resetScroll();
		minscroll = 250;
		maxscroll = 300;

	}

	public static void menu() throws InterruptedException, IOException, ClassNotFoundException {
		while (!jugar) {
			ins = false;
			f.background = "Fondo2.jpg";
			x = f.getCurrentMouseX();
			y = f.getCurrentMouseY();
			if (cargar) {
				cargar1();
			}
			menu.add((new Botones("Jugar", 25, 350, 325, 400, "RANKING.png")));
			menu.add((new Botones("Jugar", 25, 450, 325, 500, "JUGAR.png")));
			menu.add((new Botones("Jugar", 25, 550, 325, 600, "INS.png")));
			menu.add((new Botones("Jugar", 25, 650, 325, 700, "CARGAR.png")));
			

			for (Botones s : menu) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "JUGAR.png") {
						jugar = true;
					}
				}
					if (s.collidesWithPoint(x, y)) {
						if (s.path == "INS.png") {
							while (!ins) {
								f.clear();
								instrucciones();
							}

						}
					}
						if (s.collidesWithPoint(x, y)) {
							if (s.path == "CARGAR.png") {
								cargar = true;
							}

						}
						if (s.collidesWithPoint(x, y)) {
							if (s.path == "RANKING.png") {
								Ranking.leerScore("Ranking.csv");							
								}
						}
				}
			
					f.draw(menu);
					Thread.sleep(100);
		}
}

	private static void cargar1() throws ClassNotFoundException, IOException {
		File f2 = new File("PartidaGuardada.txt");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardado g = (Guardado) ois.readObject();
		x = 204;
		y = 476;
		if (g.jugar) {
			jugar = true;
		} else if (g.jugar2) {
			jugar2 = true;
		} else if (g.jugar3) {
			jugar3 = true;
		}
		ois.close();
	}

	private static void cargar2() throws ClassNotFoundException, IOException {
		File f2 = new File("PartidaGuardada.txt");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardado g = (Guardado) ois.readObject();
		Ragnar.getRagnar().x1 = g.x1;
		Ragnar.getRagnar().x2 = g.x2;
		Ragnar.getRagnar().y1 = g.y1;
		Ragnar.getRagnar().y2 = g.y2;
		puntuacion = g.puntuacion;
		Ragnar.getRagnar().hp = g.hp;
		m.enemigos = g.enemigos;
		m.tierras = g.tierras;
		f.scroll(g.scrollx, g.scrolly);
		if (g.jugar) {
			m.tiros = g.tiros;
			m.tirosE = g.tirosE;
			portal = g.portal;
			trans = g.trans;
		} else if (g.jugar2) {
			apple = g.apple;
			Terreno.caida = g.caida;
			Terreno.caida2 = g.caida2;
			b.x1 = g.bx1;
			b.x2 = g.bx2;
			b.y1 = g.by1;
			b.y2 = g.by2;
		}else if (g.jugar3) {
			m.tirosE = g.tirosE;
		}
		cargar = false;
		ois.close();

	}

	private static void instrucciones() throws InterruptedException {
		f.background = "Lobby.png";
		ArrayList<Sprite> instru = new ArrayList<>();
		instru.add((new Sprite("Jugar", 25, 550, 325, 600, "ATRAS.png")));
		int x = f.getCurrentMouseX();
		int y = f.getCurrentMouseY();
		for (Sprite s : instru) {
			if (s.collidesWithPoint(x, y)) {
				if (s.path == "ATRAS.png") {
					ins = true;

				}
			}
		}
		f.draw(instru);
		Thread.sleep(50);
	}

	public static void jugar() throws InterruptedException, IOException, ClassNotFoundException {
		Ragnar r = Ragnar.getRagnar();
		r.x1 = 175;
		r.x2 = 250;
		r.y1 = 450;
		r.y2 = 600;
		r.hp = 3;
		r.useImgArray = true;
		hud.useImgArray = true;
//		w.playMusic("");
		r.currentImg = 1;
		jugar = false;
		r.currentImg = 1;
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		m.tiros = new ArrayList<>();
		m.tirosE = new ArrayList<>();
		trans = false;
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		flag = false;
		portal = false;
		m.enemigos.add((new Enemigos("Enemigo", 1700, 100, 1800, 300, "baile.png")));
		m.enemigos.add((new Enemigos("EnemigoF", 2500, 200, 2700, 530, "baile.png")));
		m.tierras.add((new Terreno("suelo", 100, 600, 800, 670, suelitos)));
		m.tierras.add((new Terreno("suelo", 400, 530, 600, 600, suelitos)));
		m.tierras.add((new Terreno("suelo", 600, 530, 800, 600, suelitos)));
		m.tierras.add((new Terreno("suelo", 600, 470, 800, 530, suelitos)));
		m.tierras.add((new Terreno("suelo", 1000, 300, 1900, 370, suelitos)));
		m.tierras.add((new Terreno("suelo", 2000, 530, 3000, 600, suelitos)));
		if (cargar) {
			cargar2();
		}
		while (!flag) {
			f.background = "fondo.jpg";
			t = new Textito("Score", 1250, 0, 1400, 50, "Puntos: " + puntuacion);
			Enemigos.moveE();
			r.update();
			Enemigos.disparoE();
			input();
			Enemigos.gravedadE();
			ProyectilE.colisionP();
			Enemigos.saltoE();
			r.marcadorV(hud);
			if (trans) {
				r.SinGravedad();
			} else {
				r.gravedad();
			}
			r.RagnarTocao();
			GoPortal();
			for (Proyectil p : m.tiros) {
				p.move(f);
			}
			for (ProyectilE pE : m.tirosE) {
				pE.move(f);
			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			sprites.addAll(m.tiros);
			sprites.addAll(m.tirosE);
			sprites.add(hud);
			sprites.add(t);
			for (Iterator iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
	}

	private static void GoPortal() {
		for (Enemigos e : m.enemigos) {
			if (e.delete) {
				if (e.name == "EnemigoF" && e.delete) {
					if (!portal) {
						portal();
					}
				}
			}
		}
		for (Terreno t : m.tierras) {
			if (t.name == "portal" && Ragnar.getRagnar().collidesWith(t)) {
				jugar = false;
				flag = true;
				jugar2 = true;

			}

		}
	}

	private static void GoApple() {
		for (Enemigos e : m.enemigos) {
			if (e.delete) {
				if (e.name == "PatoF" && e.delete) {
					if (!apple) {
						apple();// TODO Auto-generated method stub
					}
				}
			}
		}
	}

	private static void apple() {
		for (Enemigos e : m.enemigos) {
			m.tierras.add((new Terreno("apple", 8150, 600, 8250, 650, "calabaza.png")));
			apple = true;
		}

	}

	private static void portal() {
		m.tierras.add((new Terreno("portal", 2800, 200, 3000, 530, "valhalla.jpg")));
		portal = true;

	}

	/**
	 * Toda la lista de movimientos y acciones que puede hacer nuestro personaje
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	private static void input() throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		// devuelve un set con todas las teclas apretadas
		if (w.getPressedKeys().contains('a')) {
			Ragnar.getRagnar().currentImg = 2;
			Ragnar.getRagnar().moveIzq(f);
			if (Ragnar.getRagnar().x1 < minscroll) {

				// scroll positiu: A la dreta.
				f.scroll(5, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscroll -= 5;
				maxscroll -= 5;
			}
		} else if (w.getPressedKeys().contains('d')) {
			Ragnar.getRagnar().currentImg = 1;
			Ragnar.getRagnar().moveDer(f);
			if (Ragnar.getRagnar().x1 > maxscroll) {
				// scroll positiu: A l'esquerra
				f.scroll(-5, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscroll += 5;
				maxscroll += 5;
			}
		}

		if (w.getPressedKeys().contains('w')) {
			if (Ragnar.getRagnar().isOnCeiling(f)) {

			} else {
				Ragnar.getRagnar().jump(18, 15);
			}
		}
		if (w.getPressedKeys().contains(' ')) {
			Proyectil p = (Ragnar.getRagnar().shoot());
			if (p != null) {
				m.tiros.add(p);
				p.currentImg = 1;
			}

		}
		if (w.getPressedKeys().contains('p')) {
			Ragnar.getRagnar().espadazo();

		} else if (w.getPressedKeys().contains('l')) {
			Ragnar.getRagnar().currentImg = 5;
			Ragnar.getRagnar().RagnarTocao();

		} else if (w.getPressedKeys().contains('r')) {
			Proyectil pR = (Ragnar.getRagnar().shootR());
			if (pR != null) {
				m.tiros.add(pR);
				pR.currentImg = 2;
			}

		} else if (w.getPressedKeys().contains('o')) {
			if (w.getKeyPressed() == 'o') {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				Ragnar.getRagnar().tranformacion();
			}
		} else if (w.getPressedKeys().contains('s') && !trans) {
			Ragnar.getRagnar().pabajo();
		} else if (!w.getPressedKeys().contains('s')) {
			Ragnar.getRagnar().parriba();
		}
		if (w.getPressedKeys().contains((char) 27)) {
			if (w.getKeyPressed() == ((char) 27)) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				pause = true;
				ingameMenu();
			}
		}
	}

	public static void ingameMenu() throws InterruptedException, IOException, ClassNotFoundException {
		while (pause) {

			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			menu.add((new Botones("Continuar", 625, 450, 925, 500, "JUGAR.png")));
			menu.add((new Botones("Guardar", 625, 550, 925, 600, "GUARDAR.png")));
//			menu.add((new Botones("Salir", 625, 650, 925, 700, "SALIR.png")));

//			if (w.getPressedKeys().contains((char)27)) {
//				if (w.getKeyPressed() == ((char)27)) {
//					try {
//						Thread.sleep(100);
//					} catch (InterruptedException e) {
//
//						e.printStackTrace();
//					}
//					pause=false;
//				}

			for (Botones s : menu) {
				if (s.collidesWithPoint(x, y)) {

					if (s.path == "GUARDAR.png") {

						guardar(Ragnar.getRagnar().x1, Ragnar.getRagnar().x2, Ragnar.getRagnar().y1,
								Ragnar.getRagnar().y2, puntuacion, Ragnar.getRagnar().hp, m.enemigos, m.tierras,
								m.tiros, m.tirosE, jugar, portal, flag, jugar2, jugar3, trans, f.getScrollx(),
								f.getScrolly(), Terreno.caida, Terreno.caida2, b.x1, b.x2, b.y1, b.y2);
					}
					if (s.collidesWithPoint(x, y)) {
						if (s.path == "JUGAR.png") {
							pause = false;
						}

					}
//					if (s.collidesWithPoint(x, y)) {
//						if (s.path == "SALIR.png") {
//							flag = true;
//							salirjugar=true;
//							
//							jugar=false;
//							jugar2=false;
//							jugar3=false;
//						}
//
//					}
				}
			}
			f.draw(menu);
			Thread.sleep(100);
		}
	}

	public static void guardar(int x1, int x2, int y1, int y2, int puntuacion, int hp, ArrayList<Enemigos> enemigos,
			ArrayList<Terreno> tierras, ArrayList<Proyectil> tiros, ArrayList<ProyectilE> tirosE, boolean jugar,
			boolean portal, boolean flag, boolean jugar2, boolean jugar3, boolean trans, int scrollx, int scrolly,
			boolean caida, boolean caida2, int bx1, int bx2, int by1, int by2)
			throws IOException, InterruptedException {

		if (Vikings.jugar) {
			Guardado g = new Guardado(x1, x2, y1, y2, puntuacion, hp, enemigos, tierras, tiros, tirosE, jugar, portal,
					flag, jugar2, jugar3, trans, scrollx, scrolly);
			File f2 = new File("PartidaGuardada.txt");
			FileOutputStream fos = new FileOutputStream(f2);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(g);
			oos.flush();
			oos.close();
			Thread.sleep(150);
			System.out.println("Guardado con exito");
		}
		if (Vikings.jugar2) {
			Guardado g = new Guardado(x1, x2, y1, y2, puntuacion, hp, enemigos, tierras, jugar3, trans, jugar, portal,
					flag, jugar2, jugar3, caida, caida2, bx1, bx2, by1, by2, scrollx, scrolly);
			File f2 = new File("PartidaGuardada.txt");
			FileOutputStream fos = new FileOutputStream(f2);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(g);
			oos.flush();
			oos.close();
			Thread.sleep(150);
			System.out.println("Guardado con exito");
		} else if (Vikings.jugar3) {
			Guardado g = new Guardado(x1, x2, y1, y2, puntuacion, hp, enemigos, tierras, tirosE, jugar3, scrollx, scrolly);
			File f2 = new File("PartidaGuardada.txt");
			FileOutputStream fos = new FileOutputStream(f2);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(g);
			oos.flush();
			oos.close();
			Thread.sleep(150);
			System.out.println("Guardado con exito");
		}
	}
//	public static void cargar() throws IOException, ClassNotFoundException {
//		File f2 = new File("PartidaGuardada.txt");
//		FileInputStream fis = new FileInputStream(f2);
//		ObjectInputStream ois = new ObjectInputStream(fis);
//		Guardado g = (Guardado) ois.readObject();
//		Ragnar.getRagnar();
//		int puntuacion;
//		
//		System.out.println(g);
//		ois.close();
//	}
}